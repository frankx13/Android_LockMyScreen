package com.studio.neopanda.lockmyscreen;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    private static int GALLERY_REQUEST_CODE = 1;
    //UI
    @BindView(R.id.image_container)
    ImageView imageContainer;
    @BindView(R.id.lock_control_btn)
    Button lockController;
    @BindView(R.id.image_picker_gallery_btn)
    Button imagePickerGallery;
    @BindView(R.id.image_picker_url_btn)
    Button imagePickerURL;
    @BindView(R.id.url_user_input)
    EditText urlInput;
    @BindView(R.id.validation_image_btn)
    Button validate_btn;
    @BindView(R.id.image_size_controller)
    RelativeLayout sizeController;
    @BindView(R.id.image_size_controller_down)
    Button sizeControllerDown;
    @BindView(R.id.image_size_controller_up)
    Button sizeControllerUp;
    @BindView(R.id.image_size_controller_left)
    Button sizeControllerLeft;
    @BindView(R.id.image_size_controller_right)
    Button sizeControllerRight;

    //DATA
    private String url = "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0f/Grosser_Panda.JPG/290px-Grosser_Panda.JPG";
    private int imageType = 0;
    private String uriOfImageGallery = "";
    private Bitmap urlOfImageWeb;
    private boolean isLocked = false;
    private float scaleImageY = 1.0f;
    private float scaleImageX = 1.0f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        pickImgGallery();
        pickImgURL();
        validateChoice();
        lockControllerInterface();
        sizeControllerInterface();
    }

    private void pickImgGallery() {
        imagePickerGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 1;
                validate_btn.setVisibility(View.VISIBLE);
                imageContainer.setVisibility(View.VISIBLE);
                pickFromGallery();
            }
        });
    }

    private void pickImgURL() {
        imagePickerURL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 2;
                imagePickerGallery.setVisibility(View.GONE);
                imagePickerURL.setVisibility(View.GONE);
                imageContainer.setVisibility(View.VISIBLE);
                validate_btn.setVisibility(View.VISIBLE);
                urlInput.setVisibility(View.VISIBLE);
            }
        });
    }

    private void validateChoice() {
        validate_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imageType == 1) {
                    sizeController.setVisibility(View.VISIBLE);
                    imagePickerGallery.setVisibility(View.GONE);
                    imagePickerURL.setVisibility(View.GONE);
                    validate_btn.setVisibility(View.GONE);
                    lockController.setVisibility(View.VISIBLE);
                    imageContainer.setImageURI(Uri.parse(uriOfImageGallery));

                } else if (imageType == 2) {
                    if (!urlInput.getEditableText().toString().equals("")) {
                        sizeController.setVisibility(View.VISIBLE);
                        validate_btn.setVisibility(View.GONE);
                        urlInput.setVisibility(View.GONE);
                        lockController.setVisibility(View.VISIBLE);
                        url = urlInput.getEditableText().toString();
                        urlOfImageWeb = getImageBitmap(url);
                        imageContainer.setImageBitmap(urlOfImageWeb);
                    } else {
                        Toast.makeText(MainActivity.this,
                                "You must provide an URL path !",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    //Gallery image type
    private void pickFromGallery() {
        Intent intent;
        intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/*");
        startActivityForResult(intent, GALLERY_REQUEST_CODE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result code is RESULT_OK only if the user selects an Image
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GALLERY_REQUEST_CODE) {
                Uri selectedImage = data.getData();
                if (selectedImage != null) {
                    uriOfImageGallery = selectedImage.toString();
                    imageContainer.setImageURI(Uri.parse(uriOfImageGallery));
                }
            }
        }
    }


    //URL image type
    private Bitmap getImageBitmap(String url) {
        Bitmap bm = null;
        try {
            URL aURL = new URL(url);
            URLConnection conn = aURL.openConnection();
            conn.connect();
            InputStream is = conn.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
            bm = BitmapFactory.decodeStream(bis);
            bis.close();
            is.close();
        } catch (IOException e) {
            Log.e("ERROR BITEMAP", "Error getting bitmap", e);
        }
        return bm;
    }

    private void lockControllerInterface() {
        lockController.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isLocked) {
                    sizeController.setVisibility(View.VISIBLE);
                    isLocked = false;
                    enableUserInteraction();
                    lockController.setText(getResources().getString(R.string.lock_the_screen));
                } else {
                    sizeController.setVisibility(View.GONE);
                    isLocked = true;
                    disableUserInteraction();
                    lockController.setText(getResources().getString(R.string.unlock_the_screen));
                }
            }
        });
    }

    private void disableUserInteraction() {
//        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
//                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        imageContainer.setClickable(false);
        imageContainer.setFocusable(false);
    }

    private void enableUserInteraction() {
        this.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void sizeControllerInterface() {
        sizeControllerDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageContainer.setScaleY(scaleImageY -= 0.1f);
            }
        });
        sizeControllerUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageContainer.setScaleY(scaleImageY += 0.1f);
            }
        });
        sizeControllerLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageContainer.setScaleX(scaleImageX -= 0.1f);
            }
        });
        sizeControllerRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageContainer.setScaleX(scaleImageX += 0.1f);
            }
        });
    }
}
